#include <WiFi.h>
#include <WiFiUdp.h>

#include "driver/can.h"

// #define _DEBUG_CAN

#define TX_GPIO_NUM GPIO_NUM_21
#define RX_GPIO_NUM GPIO_NUM_22
#define SPEED_PIN GPIO_NUM_4
#define LED GPIO_NUM_2
#define CAN_RECOVERY_TASK_PRIO 5
#define BLINK_TASK_PRIO 6
#define CAN_ALERT_TASK_PRIO 4
#define RX_TASK_PRIO 7
#define TX_TASK_PRIO 8
#define UDP_RX_PRIO 9
#define RPM_PER_KPH 6.73
#define MAX_KPH 250.0
#define MAX_RPM 6000.0
#define LEDC_CHANNEL 0

#define UDP_TX_PACKET_MAX_SIZE 8192

#define SSID "Baums Netz"
#define PSK "9752311311828309"

int LEDState = HIGH;
const float FactRPM = 0.025;          // 25 per 1000 rpm
const int tx_interval = 40;           // interval at which to send CAN Messages (ms)
const int udp_rx_interval = 40;       // ms
const int recovery_interval = 1000;   // ms
const int rx_interval = 100;          // interval at which to receive CAN Messages (ms)
const int blink_interval = 500;       // ms
const int alert_read_interval = 500;  // ms
const unsigned int localPort = 4444;  // local port to listen on

void IRAM_ATTR onTimer();

static const can_timing_config_t t_config = CAN_TIMING_CONFIG_500KBITS();
static const can_filter_config_t f_config = CAN_FILTER_CONFIG_ACCEPT_ALL();
can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(TX_GPIO_NUM, RX_GPIO_NUM, CAN_MODE_NORMAL);

// Buffer for receiving data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1];

HardwareSerial MySerial2(2);
WiFiUDP Udp;

struct __attribute__((packed)) outgauge_t {
   unsigned time;         // time in milliseconds (to check order)
   char car[4];           // Car name
   unsigned short flags;  // Info (see OG_x below)
   char gear;             // Reverse:0, Neutral:1, First:2...
   char plid;             // Unique ID of viewed player (0 = none)
   float speed;           // M/S
   float rpm;             // RPM
   float turbo;           // BAR
   float engTemp;         // C
   float fuel;            // 0 to 1
   float oilPressure;     // BAR
   float oilTemp;         // C
   unsigned dashLights;   // Dash lights available (see DL_x below)
   unsigned showLights;   // Dash lights currently switched on
   float throttle;        // 0 to 1
   float brake;           // 0 to 1
   float clutch;          // 0 to 1
   char display1[16];     // Usually Fuel
   char display2[16];     // Usually Settings
   int id;                // optional - only if OutGauge ID is specified
};

struct DashLight {
   enum Enum {
      SHIFT = 1 << 0,
      FULLBEAM = 1 << 1,
      HANDBRAKE = 1 << 2,
      TC = 1 << 4,
      SIGNAL_L = 1 << 5,
      SIGNAL_R = 1 << 6,
      SIGNAL_ANY = 1 << 7,
      OILWARN = 1 << 8,
      BATTERY = 1 << 9,
      ABS = 1 << 10,
      LOWBEAM = 1 << 11,
   };
};

// Global outgauge_t struct
outgauge_t gaugeState;

struct LightFlag0 {
   enum Enum {
      HighBeams = 0x04,
      FogLightsFront = 0x08,
      FogLightsRear = 0x10,
      TurnSig_L = 0x20,
      TurnSig_R = 0x40,
      TurnSig_Warn = TurnSig_L | TurnSig_R
   };
};

struct LightFlag3 {
   enum Enum {
      BrokenBulbRear_R = 0x04,
      BrokenBulbRear_L = 0x08,
      BrokenBulbFront_R = 0x10,
      BrokenBulbFront_L = 0x20,
      Car = 0x80,
   };
};

void connectToWifi() {
   WiFi.mode(WIFI_STA);
   WiFi.begin(SSID, PSK);
   while (WiFi.status() != WL_CONNECTED) {
      Serial.print('.');
      delay(500);
   }
   Serial.print("Connected! IP address: ");
   Serial.println(WiFi.localIP());
}

static void blink_task(void* arg) {
   const TickType_t xDelay = blink_interval / portTICK_PERIOD_MS;
   while (true) {
      digitalWrite(LED, LEDState);
      LEDState = LEDState ^ 1;
      vTaskDelay(xDelay);
   }
}

static void can_alert_task(void* arg) {
   uint32_t alerts_triggered, mask;
   const TickType_t xDelay = blink_interval / portTICK_PERIOD_MS;

   while (true) {
      // Block indefinitely until an alert occurs
      can_read_alerts(&alerts_triggered, pdMS_TO_TICKS(1000));
#ifdef _DEBUG_CAN
      mask = 1;
      while (mask) {
         switch (alerts_triggered & mask) {
            case CAN_ALERT_TX_SUCCESS:
               Serial.println("CAN alert: The previous transmission was successful.");
               break;

            case CAN_ALERT_ERR_ACTIVE:
               Serial.println("CAN alert: CAN controller has become error active.");
               break;

            case CAN_ALERT_RECOVERY_IN_PROGRESS:
               Serial.println("CAN alert: CAN controller is undergoing bus recovery.");
               break;

            case CAN_ALERT_BUS_RECOVERED:
               Serial.println("CAN alert: CAN controller has successfully completed bus recovery.");
               break;

            case CAN_ALERT_ARB_LOST:
               Serial.println("CAN alert: The previous transmission lost arbitration.");
               break;

            case CAN_ALERT_ABOVE_ERR_WARN:
               Serial.println("CAN alert: One of the error counters have exceeded the error warning limit.");
               break;

            case CAN_ALERT_BUS_ERROR:
               Serial.println("CAN alert: A (Bit, Stuff, CRC, Form, ACK) error has occurred on the bus.");
               break;

            case CAN_ALERT_TX_FAILED:
               Serial.println("CAN alert: The previous transmission has failed.");
               break;

            case CAN_ALERT_RX_QUEUE_FULL:
               Serial.println("CAN alert: The RX queue is full causing a received frame to be lost.");
               break;

            case CAN_ALERT_ERR_PASS:
               Serial.println("CAN alert: CAN controller has become error passive.");
               break;

            case CAN_ALERT_BUS_OFF:
               Serial.println("CAN alert: Bus-off condition occurred. CAN controller can no longer influence bus.");
               break;
         }
         mask <<= 1;
      }
      Serial.println("can_alert_task: calling vTaskDelay()");
#endif
      vTaskDelay(xDelay);
   }
}

static void can_recovery_task(void* arg) {
   can_status_info_t status_info;
   const TickType_t xDelay = recovery_interval / portTICK_PERIOD_MS;

   while (true) {
      if (can_get_status_info(&status_info) == ESP_OK) {
         switch (status_info.state) {
            case CAN_STATE_STOPPED:
#ifdef _DEBUG_CAN
               Serial.println("CAN driver state: Stopped state. The CAN controller will not participate in any CAN bus activities.");
#endif
               break;

            case CAN_STATE_RUNNING:
#ifdef _DEBUG_CAN
               Serial.println("CAN driver state: Running state. The CAN controller can transmit and receive messages.");
#endif
               break;

            case CAN_STATE_BUS_OFF:
               Serial.println("CAN driver state: Bus-off state. The CAN controller cannot participate in bus activities until it has recovered.\nAttempting to recover now...");
               if (can_initiate_recovery() != ESP_OK) {
                  Serial.println("can_recovery_task: Bus recovery process could not be started.");
               }
               break;

            case CAN_STATE_RECOVERING:
#ifdef _DEBUG_CAN
               Serial.println("CAN driver state: Recovering state. The CAN controller is undergoing bus recovery.");
#endif
               break;
         }
      }
      else {
         Serial.println("can_recovery_task: Failed to get status info.");
      }
      vTaskDelay(xDelay);
   }
}

static void can_receive_task(void* arg) {
   const TickType_t xDelay = rx_interval / portTICK_PERIOD_MS;

   while (true) {
      // Wait for message to be received
      can_message_t message;
      if (can_receive(&message, pdMS_TO_TICKS(5000)) == ESP_OK) {
#ifdef _DEBUG_CAN
         Serial.println("can_receive_task: Message received");
#endif
      }
      else {
         Serial.println("can_receive_task: Failed to receive message");
         continue;
      }

#ifdef _DEBUG_CAN
      // Process received message
      if (message.flags & CAN_MSG_FLAG_EXTD) {
         Serial.println("can_receive_task: Message is in Extended Format");
      }
      else {
         Serial.println("can_receive_task: Message is in Standard Format");
      }
#endif

      /*
      printf("ID is %d\n", message.identifier);
      if (!(message.rtr)) {
         for (int i = 0; i < message.data_length_code; i++) {
            printf("Data byte %d = %d\n", i, message.data[i]);
         }
      }
      // */

      vTaskDelay(xDelay);
   }
}

void logCanErr(esp_err_t res) {
   switch (res) {
      case ESP_OK:
         Serial.println("can_transmit: Transmission successfully queued/initiated");
         break;

      case ESP_ERR_INVALID_ARG:
         Serial.println("can_transmit: Arguments are invalid");
         break;

      case ESP_ERR_TIMEOUT:
         Serial.println("can_transmit: Timed out waiting for space on TX queue");
         break;

      case ESP_FAIL:
         Serial.println("can_transmit: TX queue is disabled and another message is currently transmitting");
         break;

      case ESP_ERR_INVALID_STATE:
         Serial.println("can_transmit: CAN driver is not in running state, or is not installed");
         break;

      case ESP_ERR_NOT_SUPPORTED:
         Serial.println("can_transmit: Listen Only Mode does not support transmissions");
         break;
   }
}

static void can_transmit_task(void* arg) {
   float fRpm = 1000.0;
   const TickType_t xDelay = tx_interval / portTICK_PERIOD_MS;

   while (true) {
      fRpm = std::max(0.0f, std::min((float)MAX_RPM, gaugeState.rpm));

      // Send CAN Messages:

      // DME1
      can_message_t tx_frame;
      tx_frame.identifier = 0x316;
      tx_frame.data_length_code = 8;
      tx_frame.data[0] = 0x05;
      tx_frame.data[1] = 0x62;
      tx_frame.data[2] = 0xFF;                       // LSB
      tx_frame.data[3] = (uint8_t)(fRpm * FactRPM);  // MSB
      tx_frame.data[4] = 0x65;
      tx_frame.data[5] = 0x12;
      tx_frame.data[6] = 0x00;
      tx_frame.data[7] = 0x3E;
      esp_err_t res = can_transmit(&tx_frame, pdMS_TO_TICKS(1000));

#ifdef _DEBUG_CAN
      logCanErr(res);
#endif

      // DME2
      tx_frame.identifier = 0x329;
      tx_frame.data[0] = 0;
      tx_frame.data[1] = (uint8_t)((gaugeState.engTemp + 48.0f) * (4.0f / 3.0f));  // °C = 0.75 * x - 48
      tx_frame.data[2] = 0;
      tx_frame.data[3] = 0x04;  // Bit 2 hardcoded to 1
      tx_frame.data[4] = 0;
      tx_frame.data[5] = 0;
      tx_frame.data[6] = 0;
      tx_frame.data[7] = 0;
      res = can_transmit(&tx_frame, pdMS_TO_TICKS(1000));

#ifdef _DEBUG_CAN
      logCanErr(res);
#endif

      vTaskDelay(xDelay);
   }
}

void udpReceive() {
   // If there's data available, read a packet
   int packetSize = Udp.parsePacket();
   if (!packetSize) return;

   // Read the packet into packetBuffer and update the global gauge state
   int length = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
   if (length < sizeof(gaugeState)) {
      Serial.println("udpReceive(): Insufficient data received.");
   }
   else {
      gaugeState = *(outgauge_t*)packetBuffer;
   }
   vTaskDelay(10);
}

static void udp_receive_task(void* arg) {
   const TickType_t xDelay = udp_rx_interval / portTICK_PERIOD_MS;

   while (true) {
      udpReceive();
      vTaskDelay(xDelay);
   }
}

void sendKbus(byte* data) {
   int len = data[1] + 2;
   data[len - 1] = iso_checksum(data, len - 1);
   MySerial2.write(data, len);
}

byte iso_checksum(byte* data, byte len) {
   byte crc = 0;
   for (byte i = 0; i < len; i++) {
      crc = crc ^ data[i];
   }
   return crc;
}

void setLights(uint8_t *arflags) {
   byte msg[] = {0xD0, 0x08, 0xBF, 0x5B, arflags[0], arflags[1], arflags[2], arflags[3], arflags[4], 0x00};
   sendKbus(msg);
}

void setKph(float speed) {
   speed = std::max(0.0f, std::min((float)MAX_KPH, speed));
   ledcWriteTone(LEDC_CHANNEL, RPM_PER_KPH * speed);
}

void loop() {
   // Set speed
   setKph(gaugeState.speed * 3.6f);

   // Set lights
   uint8_t arLightFlags[] = {0, 0, 0, 0, 0};
   // if (gaugeState.showLights & DashLight::LOWBEAM) arLightFlags[?] |= LightFlag?::LowBeams;
   if (gaugeState.showLights & DashLight::FULLBEAM) arLightFlags[0] |= LightFlag0::HighBeams;
   if (gaugeState.showLights & DashLight::SIGNAL_L) arLightFlags[0] |= LightFlag0::TurnSig_L;
   if (gaugeState.showLights & DashLight::SIGNAL_R) arLightFlags[0] |= LightFlag0::TurnSig_R;
   setLights(arLightFlags);

   delay(tx_interval);
}

void setup() {
   Serial.begin(115200);
   MySerial2.begin(9600, SERIAL_8E1, GPIO_NUM_16, GPIO_NUM_17, true);

   // Initialize pins
   pinMode(LED, OUTPUT);
   pinMode(SPEED_PIN, OUTPUT);

   // Initialize global gauge state
   memset(&gaugeState, 0, sizeof(gaugeState));
   gaugeState.engTemp = 20.0f;

   connectToWifi();

   Serial.printf("UDP server on port %d\n", localPort);
   Udp.begin(localPort);

   // Configure can bus driver
   g_config.tx_queue_len = 5;
   g_config.rx_queue_len = 5;

   // Install can driver
   esp_err_t res = can_driver_install(&g_config, &t_config, &f_config);

   if (res == ESP_OK) {
      Serial.println("setup: CAN driver installed ok");
   }
   else {
      Serial.println("setup: error installing CAN driver");
   }

   // Start CAN driver
   res = can_start();

   if (res == ESP_OK) {
      Serial.println("setup: CAN driver started ok");
   }
   else {
      Serial.println("setup: error starting CAN driver");
   }

   // Reconfigure alerts to detect Error Passive and Bus-Off error states
   if (can_reconfigure_alerts(CAN_ALERT_ALL, NULL) == ESP_OK) {
      Serial.println("setup: Alerts reconfigured");
   }
   else {
      Serial.println("setup: Failed to reconfigure alerts");
   }

   // Create udp receive task
   xTaskCreate(can_transmit_task, "CAN transmit task", 4096, NULL, TX_TASK_PRIO, NULL);
   xTaskCreate(can_receive_task, "CAN receive task", 4096, NULL, RX_TASK_PRIO, NULL);
   xTaskCreate(can_alert_task, "CAN alert task", 4096, NULL, CAN_ALERT_TASK_PRIO, NULL);
   xTaskCreate(can_recovery_task, "CAN recovery task", 4096, NULL, CAN_RECOVERY_TASK_PRIO, NULL);
   xTaskCreate(blink_task, "Blink task", 4096, NULL, BLINK_TASK_PRIO, NULL);
   xTaskCreate(udp_receive_task, "UDP receive task", 4096, NULL, UDP_RX_PRIO, NULL);

   // Set the time to 00:00                          hh    mm
   byte msgTime[] = {0x3B, 0x06, 0x80, 0x40, 0x01, 0x00, 0x00, 0x00};
   sendKbus(msgTime);

   ledcAttachPin(SPEED_PIN, LEDC_CHANNEL);
   ledcSetup(LEDC_CHANNEL, 1000, 8);
   setKph(0.0f);

   // -------------------------------------
   // Kbus test stuff
   // -------------------------------------

   // Send Kbus
   // byte msg[] = {0xD0, 0x08, 0xBF, 0x5B, LightByte1, 0x00, 0x00, LightByte2, 0x00, 0x00};
   // byte msg[] = {0xD0, 0x08, 0xBF, 0x5B, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00};
   // sendKbus(msg);

   // High beams
   // byte msgLights[] = {0xD0, 0x08, 0xBF, 0x5B, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00};
   // sendKbus(msgLights);

   /*
   // Turnsig left start
   byte msgLights[] = {0xD0, 0x08, 0xBF, 0x5B, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00};
   sendKbus(msgLights);

   // Turnsig right start
   msgLights[4] = 0x40;
   sendKbus(msgLights);

   // Turnsig stop
   msgLights[4] = msgLights[6] = 0;
   sendKbus(msgLights);
   //*/

   /*
   // Use 1st timer of 4 (counted from zero).
   // Set 80 divider for prescaler (see ESP32 Technical Reference Manual for more
   // info).
   timer = timerBegin(0, 80, true);

   // Attach onTimer function to our timer.
   timerAttachInterrupt(timer, &onTimer, true);

   // Set alarm to call onTimer function every second (value in microseconds).
   // Repeat the alarm (third parameter)
   timerAlarmWrite(timer, 1000000, true);

   // Start an alarm
   timerAlarmEnable(timer);
   // */
}
